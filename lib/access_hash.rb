module AccessHash
  def set_access_hash
    length = 10
    char_set = ('0'..'9').to_a + ('a'..'z').to_a + ('A'..'Z').to_a
    while self.access_hash.blank? || !self.class.find_by_access_hash(access_hash).nil?
      self.access_hash = (1..length).collect { |i| char_set[rand(char_set.length)] }.join
    end
  end
end
