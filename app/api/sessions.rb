class Sessions < Grape::API
  resource :sessions do

   desc "Authenticate user and return user object / access token"

   params do
     requires :email, type: String, desc: "User email"
     requires :password, type: String, desc: "User Password"
   end

   post do
     email = params[:email]
     password = params[:password]

     if email.nil? or password.nil?
       # error!({error_code: 404, error_message: "Invalid Email or Password."},401)
       return {status: 401, error_code: 404, error_message: "Invalid Email or Password."}.to_json
     end
     
     user = User.where(email: email.downcase).first
     if user.nil?
       # error!({error_code: 404, error_message: "Invalid Email or Password."},401)
       return {status: 401, error_code: 404, error_message: "Invalid Email or Password."}.to_json
     end

     if !user.valid_password?(password)
       # error!({error_code: 404, error_message: "Invalid Email or Password."},401)
       # return
       {status: 401, error_code: 404, error_message: "Invalid Email or Password."}.to_json
     else
      user.ensure_authentication_token!
      user.save
      { status: 'ok',
        token: user.authentication_token,
        uid: user.id,
        profile_type: user.profile_type,
        profile_id: user.profile_id,
        profile_key: user.profile.access_hash
      }.to_json
     end
   end

   desc "Destroy the access token"
   params do
    requires :access_token, type: String, desc: 'access token.'
   end
   get ':access_token' do
    access_token = params[:access_token]
    user = User.where(authentication_token: access_token).first
    if user.nil?
     # error!({error_code: 404, error_message: "Invalid access token."},401)
     {status: 401, error_code: 404, error_message: "Invalid access token."}.to_json
    else
     user.reset_authentication_token
     {status: 'ok'}.to_json
    end
   end
  end
end
