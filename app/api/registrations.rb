class Registrations < Grape::API
  resource :registrations do

    desc "Authenticate user and return user object / access token"
    params do
     requires :firstname, type: String, desc: "User firstname"
     requires :lastname, type: String, desc: "User lastname"
     requires :email, type: String, desc: "User email"
     requires :password, type: String, desc: "User Password"
     requires :password_confirmation, type: String, desc: "User password_confirmation"
    end

    post do
      first_name = params[:firstname]
      last_name = params[:lastname]
      email = params[:email]
      password = params[:password]
      password_confirmation = params[:password_confirmation]
      profile_type = (params[:profile_type] || 'patient').to_s.humanize

      if first_name.nil? || last_name.nil? || email.nil? || password.nil?
        # error!({error_code: 404, error_message: "Invalid Email or Password."},401)
        return {status: 401, error_code: 404, error_message: "Invalid Email or Password."}.to_json
      end

      unless password == password_confirmation
        return {status: 401, error_code: 404, error_message: "Password not matched."}.to_json
      end

      user = User.new({
          first_name: first_name,
          last_name: last_name,
          email: email,
          password: password,
          profile_type: profile_type
        })
      profile_params = if profile_type == 'Doctor'
        {first_name: first_name, last_name: last_name, license: params[:license]}
      else
        {first_name: first_name, last_name: last_name}
      end

      if user.save
        profile = profile_type.constantize.create(profile_params)
        user.profile_id = profile.id
        user.ensure_authentication_token!
        user.save
        { status: 'ok',
          token: user.authentication_token,
          uid: user.id,
          profile_type: user.profile_type,
          profile_id: user.profile_id,
          profile_key: user.profile.access_hash
        }.to_json
      else
        {status: 401, error_code: 404, error_message: "Problem while creating account."}.to_json
      end
    end

  end
end
