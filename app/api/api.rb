class API < Grape::API
  prefix 'api'
  format :json
  
  helpers do
    def warden
      env['warden']
    end

    def authenticated
      # return true if warden.authenticated?
      # params[:access_token] && @user = User.find_by_authentication_token(params[:access_token])
      if access_token = find_access_token
        @user = User.find_by_authentication_token(access_token)
        true
      else
        false
      end
    end

    def current_user
      @user
    end

    private

    def find_access_token
      @access_token ||= request.headers['X-Auth-Token']
    end
  end

  mount Registrations
  mount Sessions
  mount V1::BaseAPI
end
