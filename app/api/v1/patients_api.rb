class V1::PatientsAPI < Grape::API
  resource :patients do
    desc 'Get all patients'
    get do
      patients = Patient.all
      {status: 'ok', patients: patients}.to_json
    end

    desc 'Get a patient'
    get '/:id' do
      patient = Patient.find(params[:id])
      {status: 'ok', patient: patient}.to_json
    end

    desc 'Update a patient profile'
    post '/:id' do
      patient = Patient.find(params[:id])
      # patient.Update
      {status: 'ok', patient: patient}.to_json
    end

    desc 'Get appointments'
    get '/:id/appointments' do
      patient = Patient.find(params[:id])
      appointments = patient.appointments.available
      {status: 'ok', appointments: appointments}.to_json
    end

    desc 'Set appointment'
    post '/:id/appointments' do
    end
  end

end
