class V1::UsersAPI < Grape::API
  resource :users do
    desc 'Get all users'
    get do
      users = User.all
      {status: 'ok', users: users}.to_json
    end
  end
end
