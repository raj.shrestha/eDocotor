class V1::DoctorsAPI < Grape::API
  resource :doctors do
    desc 'Get all doctors'
    get do
      doctors = Doctor.all.select('id', 'access_hash', 'first_name', 'last_name')
      {status: 'ok', doctors: doctors}.to_json
    end

    desc 'Get a doctor'
    get '/:id' do
      doctor = Doctor.find(params[:id])
      {status: 'ok', doctor: doctor}.to_json
    end

    desc 'Update a doctor profile'
    post '/:id' do
      doctor = Doctor.find(params[:id])
      # doctor.Update
      {status: 'ok', doctor: doctor}.to_json
    end

    desc 'Search doctors'
    get '/:search' do

    end

    desc 'Get appointments'
    get '/:id/appointments' do
      doctor = Doctor.find(params[:id])
      appointments = doctor.appointments
      {status: 'ok', appointments: appointments}.to_json
    end

    desc 'Set appointment'
    post '/:id/appointments' do
    end

    desc 'approve or shedule appointment'
    post '/:id/appointments/:appoint_id' do
    end
  end

  resource :specializations do
    format :json
    desc 'Get specializations'
    get do
      specializations = Specialization.all
      {status: 'ok', specializations: specializations}.to_json
    end
  end
end
