class V1::BaseAPI < Grape::API
  version 'v1', using: :path
  format :json

  # rescue_from :all, backtrace: true
  rescue_from ActiveRecord::RecordNotFound do
    rack_response({ 'message' => '404 Not found' }.to_json, 404)
  end

  rescue_from :all do |exception|
    # lifted from https://github.com/rails/rails/blob/master/actionpack/lib/action_dispatch/middleware/debug_exceptions.rb#L60
    # why is this not wrapped in something reusable?
    trace = exception.backtrace

    message = "\n#{exception.class} (#{exception.message}):\n"
    message << exception.annoted_source_code.to_s if exception.respond_to?(:annoted_source_code)
    message << "  " << trace.join("\n  ")

    API.logger.add Logger::FATAL, message
    rack_response({ 'message' => '500 Internal Server Error' }.to_json, 500)
  end

  error_formatter :json, V1::ErrorFormatter

  before do
    error!("401 Unauthorized, 401") unless authenticated
  end

  mount V1::UsersAPI
  mount V1::DoctorsAPI
  mount V1::PatientsAPI
  mount V1::AppointmentsAPI
end
