class V1::AppointmentsAPI < Grape::API
  resource :appointments do
    desc 'Get all appointments'
    get do
      appointments = Appointment.all
      {status: 'ok', appointments: appointments}.to_json
    end

    desc 'create appointment'
    params do
     requires :patient_id, type: Integer, desc: "User firstname"
     requires :doctor_id, type: Integer, desc: "User lastname"
     requires :shedule_at, type: DateTime, desc: "User email"
    end
    post do
      appointment = Appointment.new({
        patient_id: params[:patient_id],
        doctor_id: params[:doctor_id],
        shedule_at: params[:shedule_at],
        description: params[:description]
      })
      if appointment.save
        {status: 'ok', appointment: appointment}.to_json
      else
        {status: 401, error_code: 404, error_message: "Problem while creating account."}.to_json
      end
    end

    desc 'get appointment'
    get '/:id' do
      appointment = Appointment.find(params[:id])
      if appointment
        {status: 'ok', appointment: appointment}.to_json
      else
        {status: 401, error_code: 404, error_message: "Not found."}.to_json
      end
    end

    desc 'update status'
    put '/:id' do
      appointment = Appointment.find(params[:id])
      # appointment.status = params[:status]
      if appointment
        {status: 'ok', msg: 'Successfully updated.'}.to_json
      else
        {status: 401, error_code: 404, error_message: "Not found."}.to_json
      end
    end
  end

end
