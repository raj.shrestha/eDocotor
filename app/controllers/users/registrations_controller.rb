class Users::RegistrationsController < Devise::RegistrationsController
  protected
    def after_sign_up_path_for(resource)
      user_path(current_user.id)
    end

    def after_update_path_for(resource)
      signed_in_root_path(resource)
    end
end
