class Appointment < ActiveRecord::Base
  belongs_to :patient
  belongs_to :dcotor

  IN_PROGRESS = 0   # Initial Status
  COMPLETE    = 1  # When all the jobs are completed
  CANCELED      = 2
  APPROVED    = 3  # When a job hits max skipped limit and job has no output
  DELETED     = 4

  STATUSES = {
    0 => "Processing",
    1 => "Completed",
    2 => "Canceled",
    3 => "Approved",
    4 => "Deleted",
  }

  scope :available,    ->   { where(:status => [IN_PROGRESS, APPROVED, COMPLETE]) }
  scope :in_progress,  ->   { where(:status => IN_PROGRESS) }
  scope :complete,     ->   { where(:status => COMPLETE) }
  scope :Canceled,       ->   { where(:status => CANCELED) }
  scope :approved,     ->   { where(:status => APPROVED) }
  scope :deleted,      ->   { where(:status => DELETED) }
end
