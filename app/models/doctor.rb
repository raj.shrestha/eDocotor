class Doctor < ActiveRecord::Base
  include AccessHash

  has_one :user, as: :profile, dependent: :destroy
  has_many :appointments

  before_validation :set_access_hash, :on => :create
end
