class Relationship < ActiveRecord::Base
  belongs_to :follower, class_name: "User"
  belongs_to :followed, class_name: "User"

  validates :follower_id, presence: true
  validates :followed_id, presence: true
end

# @user = User.find(params[:relationship][:followed_id])
#     current_user.follow!(@user)
# user.followed_users
#user.followers
