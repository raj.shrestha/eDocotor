class User < ActiveRecord::Base
  enum role: [:user, :doctor, :admin]
  after_initialize :set_default_role, :if => :new_record?

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  before_save :ensure_authentication_token!

  belongs_to :profile, polymorphic: true

  has_one  :user_detail
  has_one  :doctor_detail
  has_many :relationships, foreign_key: "follower_id", dependent: :destroy
  has_many :followed_users, through: :relationships, source: :followed
  has_many :reverse_relationships, foreign_key: "followed_id",
                                   class_name: 'Relationship',
                                   dependent: :destroy
  has_many :followers, through: :reverse_relationships, source: :follower

  def set_default_role
    # self.role ||= :user
    self.profile_type ||= 'Patient'
  end

  def user_type
    self.profile_type.downcase # 'admin', 'doctor' or 'patient'
    # self.profile_type.constantize.new
  end

  def ensure_authentication_token!
    self.authentication_token ||= generate_authentication_token
  end

  def reset_authentication_token
    self.authentication_token = nil
    self.save
  end

  def following?(other_user)
    relationships.find_by(followed_id: other_user.id)
  end

  def follow!(other_user)
    relationships.create!(followed_id: other_user.id)
  end

  def unfollow!(other_user)
    relationships.find_by(followed_id: other_user.id).destroy
  end

  private

    def generate_authentication_token
      loop do
        token = Devise.friendly_token
        break token unless User.where(authentication_token: token).first
      end
    end

end
