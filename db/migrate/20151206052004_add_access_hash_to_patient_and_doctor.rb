class AddAccessHashToPatientAndDoctor < ActiveRecord::Migration
  def change
    add_column :patients, :access_hash, :string
    add_column :doctors, :access_hash, :string
  end
end
