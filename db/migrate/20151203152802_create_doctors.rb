class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.string     :first_name,     null: false
      t.string     :last_name,      null:false   
      t.string     :image
      t.string     :degree
      t.string     :license,        null: false
      t.string     :work_address
      t.string     :specialization
      t.timestamps :work_started
      t.text       :bio
      t.float      :fee

      t.timestamps null: false
    end
    
    add_index :doctors, :first_name
    add_index :doctors, :last_name
    add_index :doctors, :work_address
    add_index :doctors, :specialization
    add_index :doctors, :fee
  end
end
