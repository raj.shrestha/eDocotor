class CreateUserDetails < ActiveRecord::Migration
  def change
    create_table :user_details do |t|
      t.belongs_to :user, index: true
      t.string     :image
      t.string     :address
      t.text       :bio

      t.timestamps null: false
    end
  end
end

