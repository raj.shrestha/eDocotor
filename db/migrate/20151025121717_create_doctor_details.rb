class CreateDoctorDetails < ActiveRecord::Migration
  def change
    create_table :doctor_details do |t|
      t.belongs_to :user, index: true
      t.string     :image
      t.string     :degree,         null: false
      t.string     :license,        null: false
      t.string     :work_address
      t.string     :specialization, null: false
      t.timestamps :work_started
      t.text       :bio
      t.float      :fee

      t.timestamps null: false
    end

    add_index :doctor_details, :work_address
    add_index :doctor_details, :specialization
    add_index :doctor_details, :fee
  end
end
