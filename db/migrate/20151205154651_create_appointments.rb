class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer   :patient_id
      t.integer   :doctor_id
      t.datetime  :shedule_at
      t.integer   :status, default: 0,  null: false
      t.text      :description

      t.timestamps null: false
    end
  end
end
