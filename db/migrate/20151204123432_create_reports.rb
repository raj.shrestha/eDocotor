class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.belongs_to :patient, index: true
      t.belongs_to :doctor, index: true
      t.string     :title,   null: false  
      t.text       :content
      t.string     :image

      t.timestamps null: false
    end

    add_index :reports, :title
  end
end
