class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string     :first_name,     null: false
      t.string     :last_name,      null:false   
      t.string     :image
      t.string     :address
      t.text       :bio

      t.timestamps null: false
    end
    
    add_index :patients, :first_name
    add_index :patients, :last_name
  end
end
